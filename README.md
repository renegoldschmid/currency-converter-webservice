# currency-converter-webservice

Currency Converter Webservice for Car Rental Application

## Documentation

The java-documentation can be found in /doc.

## Run the application

The application needs to be built with maven. 
You can start the application when the .jar-File is generated in your /target-folder using:

'java -jar /path/to/file.jar'

## Deploy the application

Our preferred method for deploying the application is using the Java-Environment on AWS Beanstalk.

Attention: Spring Boot default Port is 8080 but Beanstalk uses Port 5000.
In your AWS-configuration, you can set a 'SERVER_PORT'-variable with the value of 5000 to set the right port in your application.