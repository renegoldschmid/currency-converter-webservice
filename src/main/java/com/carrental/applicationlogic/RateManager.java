package com.carrental.applicationlogic;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * This class has all the functionality for the cross-rate calculations.
 * The current rates are fetched from the european central bank.
 * @author reneg
 *
 */
public class RateManager {

	/**
	 * Repository for exchange rates from the european central bank
	 */
	private static final String URL_EXCHANGE_RATES = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
	
	/**
	 * EUR exchange rate acts as basis-value
	 */
	private static final double EXCHANGE_RATE_EUR = 1.0;
	
	/**
	 * Egyptian Pound added to repository
	 */
	private static final double EXCHANGE_RATE_EGP = 17.1047;
	
	/**
	 * Map, containing all currencies and its rate to EUR
	 */
	Map<String, Double> exchangeRates;
	
	/**
	 * Default Constructor
	 */
	public RateManager() {
		this.exchangeRates = new HashMap<String, Double>();
	}
	
	/**
	 * Fetches the current rates (for conversion from EURO) from the URL provided.
	 * If the source (URL) changes, the XML structure most likely changes as well.
	 * Therefore, this method needs to be adjusted in case the source changes.
	 */
	public void fetchRates() {
	    Document doc = prepareDocumentFromURL(URL_EXCHANGE_RATES);
	    doc.getDocumentElement().normalize();
	    
	    NodeList nl = doc.getElementsByTagName("Cube");
	    this.exchangeRates.clear();
	    
	    for (int i = 0; i < nl.getLength(); i++) {
	    	Node node = nl.item(i);	
	    	if(node.hasAttributes() && node.getAttributes().getLength() == 2) {
				String currency = node.getAttributes().item(0).getNodeValue();
				double rate = Double.parseDouble(node.getAttributes().item(1).getNodeValue());
				this.exchangeRates.put(currency, rate);
			}
	    }
	    
	    this.exchangeRates.put("EUR", EXCHANGE_RATE_EUR);
	    this.exchangeRates.put("EGP", EXCHANGE_RATE_EGP);
	}
	
	/**
	 * Prepares a Document-Object from an online XML resource.
	 * @param url
	 * @return Document
	 */
	private Document prepareDocumentFromURL(String url) {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    dbFactory.setNamespaceAware(false);
	    dbFactory.setValidating(false);
	    
	    DocumentBuilder db;
	    URLConnection conn;
	    Document doc = null;
	    
		try {
			db = dbFactory.newDocumentBuilder();
			conn = new URL(url).openConnection();
			conn.addRequestProperty("Accept", "application/xml");
			doc = db.parse(conn.getInputStream());
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}
		
		return doc;
	}
	
	/**
	 * Calculate the amount of currency using cross-rate calculation
	 * @param currency
	 * @param desiredCurrency
	 * @param amount
	 * @return exchanged amount
	 */
	public double crossRateCalculation(String currency, String desiredCurrency, double amount) {
		double rateCurrency = this.exchangeRates.get(currency);
		double rateDesiredCurrency = this.exchangeRates.get(desiredCurrency);
		return (amount * (rateDesiredCurrency / rateCurrency));
	}
	
	/**
	 * Converts a given amount in USD to EUR
	 * @param usd
	 * @return value in euro
	 */
	public double exchangeDollarToEuro(double usd) {
		return (usd / this.exchangeRates.get("USD"));
	}

	/**
	 * Exchanges a given amount in euro to the given desired currency
	 * @param amountInEuro
	 * @param desiredCurrency
	 * @return Amount converted to given currency
	 */
	public double exchangeAmount(double amountInEuro, String desiredCurrency) {
		return (amountInEuro * this.exchangeRates.get(desiredCurrency));
	}
	
	/**
	 * Returns the rate for a given currency
	 * @param currency
	 * @return rate
	 */
	public double getRateByCurrency(String currency) {
		return this.exchangeRates.get(currency);
	}
}
