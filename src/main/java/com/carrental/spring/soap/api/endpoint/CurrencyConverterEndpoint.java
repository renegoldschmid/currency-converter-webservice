package com.carrental.spring.soap.api.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.carrental.soap.api.currencyconverter.GetConvertedCurrencyRequest;
import com.carrental.soap.api.currencyconverter.GetConvertedCurrencyResponse;
import com.carrental.spring.soap.api.service.CurrencyConverterService;

/**
 * Webservice Endpoint configuration class
 * @author Rene
 *
 */
@Endpoint
public class CurrencyConverterEndpoint {

	private static final String NAMESPACE = "http://www.carrental.com/soap/api/currencyconverter";
	private static final String PASSWORD = "?Um)xj^M;_7\\%`)G";
	@Autowired
	private CurrencyConverterService service;

	@PayloadRoot(namespace = NAMESPACE, localPart = "GetConvertedCurrencyRequest")
	@ResponsePayload
	public GetConvertedCurrencyResponse getConvertedCurrency(@RequestPayload GetConvertedCurrencyRequest request) {
		if (request.getPassword().equals(PASSWORD)) {
			return service.convertCurrency(request);
		} else {
			return null;
		}
	}
	
}
