package com.carrental.spring.soap.api.service;

import org.springframework.stereotype.Service;

import com.carrental.applicationlogic.RateManager;
import com.carrental.soap.api.currencyconverter.Currency;
import com.carrental.soap.api.currencyconverter.GetConvertedCurrencyRequest;
import com.carrental.soap.api.currencyconverter.GetConvertedCurrencyResponse;

@Service
public class CurrencyConverterService {
	
	/**
	 * Request-Endpoint for cross-rate calculation
	 * @param request
	 * @return exchanged currency amount
	 */
	public GetConvertedCurrencyResponse convertCurrency(GetConvertedCurrencyRequest request) {
		GetConvertedCurrencyResponse response = new GetConvertedCurrencyResponse();
		String givenCurrency = request.getCurrencyIn();
		String desiredCurrency = request.getCurrencyOut();
		double amount = request.getAmount();
		
		// Init Ratemanager
		RateManager rm = new RateManager();
		rm.fetchRates();
		
		// Exchange currencies using cross-rate calculation
		double amountInDesiredCurrency = rm.crossRateCalculation(givenCurrency, desiredCurrency, amount);
		
		Currency currency = new Currency();
		currency.setName(desiredCurrency);
		currency.setValue(amountInDesiredCurrency);
		response.setCurrency(currency);
		return response;
	}
}
